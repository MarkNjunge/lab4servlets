package com.ammv.models;

import java.util.Collections;
import java.util.List;

public class Course {
    private String code;
    private String name;
    private List<Unit> units;

    public Course(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public List<Unit> getUnits() {
        return units;
    }
}
