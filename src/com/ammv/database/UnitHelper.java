package com.ammv.database;

import com.ammv.models.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used for database interaction for Units
 */
public class UnitHelper extends BaseHelper{

    /**
     * Used to create a unit in the database
     * @param unit The unit to be created
     * @param dbTask A listener for successful and failed database operations
     */
    public static void create(Unit unit, DBConnector.DBTask dbTask) {
        try {
            if (!unitExists(unit.getCode())) {
                String sql = "INSERT INTO miniproj_units(unit_code, unit_name) VALUES (?,?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

                preparedStatement.setString(1, unit.getCode());
                preparedStatement.setString(2, unit.getName());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("There is already a unit with the code " + unit.getCode());
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }

    }

    /**
     * Find one unit in the database
     * @param unitCode The code for the unit to be searched for
     * @param failureListener A listener for failed database operations
     * @return The unit matching the code passed in
     */
    public static Unit readOne(String unitCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_units WHERE unit_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, unitCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new Unit(resultSet.getString("unit_code"), resultSet.getString("unit_name"));
            } else {
                return new Unit("error", "error");
            }
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return new Unit("error", "error");
        }
    }

    /**
     * Find all units in the database
     * @param failureListener A listener for failed database operations
     * @return A list of buildings
     */
    public static List<Unit> readAll(DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_units";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Unit> units = new ArrayList<>();
            while (resultSet.next()) {
                units.add(new Unit(resultSet.getString("unit_code"), resultSet.getString("unit_name")));
            }

            return units;
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Find all units based on a course code
     * @param courseCode The course code to be used as a filter
     * @param failureListener A listener for failed database operations
     * @return A list of units
     */
    public static List<Unit> readByCourse(String courseCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_courseunits WHERE course_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, courseCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Unit> units = new ArrayList<>();
            while (resultSet.next()) {
                String unitCode = resultSet.getString("unit_code");
                units.add(readOne(unitCode, failureListener));
            }

            return units;
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Update the value of a unit
     * @param unit The updated unit
     * @param dbTask A listener for successful and failed database operations
     */
    public static void update(Unit unit, DBConnector.DBTask dbTask) {
        try {
            String sql = "UPDATE miniproj_units SET unit_name = ? WHERE unit_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            if (unitExists(unit.getCode())) {
                preparedStatement.setString(1, unit.getName());
                preparedStatement.setString(2, unit.getCode());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("There is no unit with the code");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }

    }

    /**
     * Delete a unit
     * @param unitCode The code for the unit to be deleted
     * @param dbTask A listener for successful and failed database operations
     */
    public static void delete(String unitCode, DBConnector.DBTask dbTask) {
        try {
            if (unitExists(unitCode)) {
                String sql = "DELETE FROM miniproj_units WHERE unit_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, unitCode);

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("Unit " + unitCode + " doesn't exist");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Used to check if a unit exists
     * @param unitCode The code for the building to be searched for
     * @return boolean of whether the unit exists
     * @throws SQLException
     */
    private static boolean unitExists(String unitCode) throws SQLException {
        String sql = "SELECT * FROM miniproj_units WHERE unit_code = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, unitCode);

        ResultSet queryResults = preparedStatement.executeQuery();
        return queryResults.next();
    }
}