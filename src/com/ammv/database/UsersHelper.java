package com.ammv.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User for database interaction with users
 */
public class UsersHelper extends BaseHelper {
    /**
     * Used to add users to the database
     * @param username The desired username
     * @param password The <strong>hashed</strong> string for the user's password
     * @param dbTask A listener for successful and failed database operations
     */
    public static void addUser(String username, String password, DBConnector.DBTask dbTask) {
        try {
            if (!userExists(username)) {
                String sql = "INSERT INTO miniproj_users(username, password) VALUES(?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("There is already a user with that username.");
            }
        } catch (SQLException e) {
            dbTask.taskFailed(e.getMessage());
        }
    }

    /**
     * Used to validate a user's credentials
     * @param username The entered username
     * @param password The hash of the entered password
     * @param dbTask A listener for successful and failed database operations
     */
    public static void validateUser(String username, String password, DBConnector.DBTask dbTask) {
        try {
            if (userExists(username)) {
                String sql = "SELECT  * FROM miniproj_users WHERE username = ? AND password = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);

                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    dbTask.taskSuccessful();
                } else {
                    dbTask.taskFailed("Username or password is incorrect.");
                }
            } else {
                dbTask.taskFailed("There is no user with that username.");
            }
        } catch (SQLException e) {
            dbTask.taskFailed(e.getMessage());
        }
    }

    /**
     * Used to check if a user with the given username exists
     * @param username The username to be searched for
     * @return boolean - <code>true</code> if the user exists. <code>false</code> if the user does not exist
     * @throws SQLException - Thrown if an error occurs when interacting with the database
     */
    private static boolean userExists(String username) throws SQLException {
        String sql = "SELECT  * FROM miniproj_users WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, username);
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet.next();
    }

}
