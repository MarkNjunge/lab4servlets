package com.ammv.database;

import com.ammv.models.Course;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used for database interaction for Courses
 */
public class CourseHelper extends BaseHelper {
    /**
     * Used to create a course in the database
     *
     * @param course The course to be created
     * @param dbTask A listener for successful and failed database operations
     */
    public static void create(Course course, DBConnector.DBTask dbTask) {
        try {
            if (!courseExists(course.getCode())) {
                String sql = "INSERT INTO miniproj_courses(course_code, course_name) VALUES (?,?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, course.getCode());
                preparedStatement.setString(2, course.getName());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("A course with code " + course + " already exists.");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Add a unit to a course
     *
     * @param unitCode   The code for the unit to be added
     * @param courseCode The code for the course to which the unit is to be added
     * @param dbTask     A listener for failed and successful database operations
     */
    public static void addUnitToCourse(String unitCode, String courseCode, DBConnector.DBTask dbTask) {
        try {
            String sql = "INSERT INTO miniproj_courseunits(course_code, unit_code) VALUES (?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, courseCode);
            preparedStatement.setString(2, unitCode);

            preparedStatement.execute();
            dbTask.taskSuccessful();
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Find one Course in the database
     *
     * @param courseCode      The code for the course to be searched for
     * @param failureListener A listener for failed database connections
     * @return The course matching the code passed in
     */
    public static Course readOne(String courseCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_courses WHERE course_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, courseCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new Course(resultSet.getString("course_code"), resultSet.getString("course_name"));
            } else {
                return new Course("error", "error");
            }
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return new Course("error", "error");
        }
    }

    /**
     * Find all courses in the database
     *
     * @param failureListener A listener for failed database operations
     * @return A list of courses
     */
    public static List<Course> readAll(DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_courses";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Course> courses = new ArrayList<>();
            while (resultSet.next()) {
                courses.add(new Course(resultSet.getString("course_code"), resultSet.getString("course_name")));
            }

            return courses;
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Find courses that have a given unit
     *
     * @param unitCode        The code for the unit to be used as a filter
     * @param failureListener A listener for failed database operations
     * @return A list of courses
     */
    public static List<Course> readByUnit(String unitCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_courseunits WHERE unit_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, unitCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Course> courses = new ArrayList<>();
            while (resultSet.next()) {
                courses.add(readOne(resultSet.getString("course_code"), failureListener));
            }

            return courses;
        } catch (SQLException sqlException) {
            failureListener.taskFailed(sqlException.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Update the value of a course
     *
     * @param course The updated course
     * @param dbTask A listener for successful and failed database operations
     */
    public static void update(Course course, DBConnector.DBTask dbTask) {
        try {
            if (courseExists(course.getCode())) {
                String sql = "UPDATE miniproj_courses SET course_name = ? WHERE course_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

                preparedStatement.setString(1, course.getName());
                preparedStatement.setString(2, course.getCode());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("There is no course with code " + course.getCode());
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Delete a course
     *
     * @param courseCode The code for the course to be deleted
     * @param dbTask     A listener for successful and failed database operations
     */
    public static void delete(String courseCode, DBConnector.DBTask dbTask) {
        try {
            if (courseExists(courseCode)) {
                String sql = "DELETE FROM miniproj_courses WHERE course_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, courseCode);

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("Course " + courseCode + " doesn't exist");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Used to check if a course exists
     *
     * @param courseCode The code for the course to be searched for
     * @return boolean of whether the course exists
     * @throws SQLException
     */
    private static boolean courseExists(String courseCode) throws SQLException {
        String sql = "SELECT * FROM miniproj_courses WHERE course_code = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, courseCode);

        ResultSet queryResults = preparedStatement.executeQuery();
        return queryResults.next();
    }
}
