package com.ammv.database;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Base helper class for other helper classes. Used to provide a connection to the database
 */
public class BaseHelper {
    static Connection connection;

    static {
        try {
            connection = DBConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
