package com.ammv.database;

import com.ammv.models.Building;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used for database interaction for Buildings
 */
public class BuildingHelper extends BaseHelper{
    /**
     * Used to create a building in the database
     * @param building The building to be created
     * @param dbTask A listener for successful and failed database operations
     */
    public static void createBuilding(Building building, DBConnector.DBTask dbTask) {
        try {
            if (!buildingExists(building.getCode())) {
                String sql = "INSERT INTO miniproj_buildings(build_code, build_name) VALUES (?,?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, building.getCode());
                preparedStatement.setString(2, building.getName());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("A building with code " + building.getCode() + " already exists");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Find one building in the database
     * @param buildingCode The code for the building to be searched for
     * @param failureListener A listener for failed database operations
     * @return The building matching the code passed in
     */
    public static Building readOne(String buildingCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_buildings WHERE build_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, buildingCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new Building(resultSet.getString("build_code"), resultSet.getString("build_name"));
            }
            return new Building("error", "error");
        } catch (SQLException e) {
            failureListener.taskFailed(e.getMessage());
            return new Building("error", "error");
        }
    }

    /**
     * Find all buildings in the database
     * @param failureListener A listener for failed database operations
     * @return A list of buildings
     */
    public static List<Building> readAll(DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_buildings";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            List<Building> buildings = new ArrayList<>();
            while (resultSet.next()) {
                buildings.add(new Building(resultSet.getString("build_code"), resultSet.getString("build_name")));
            }
            return buildings;
        } catch (SQLException e) {
            failureListener.taskFailed(e.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Update the value of a building
     * @param building The updated building
     * @param dbTask A listener for successful and failed database operations
     */
    public static void update(Building building, DBConnector.DBTask dbTask) {
        try {
            if (buildingExists(building.getCode())) {
                String sql = "UPDATE miniproj_buildings SET build_name = ? WHERE build_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, building.getName());
                preparedStatement.setString(2, building.getCode());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("This record doesn't exist!");
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Delete a building
     * @param buildingCode The code for the building to be deleted
     * @param dbTask  A listener for successful and failed database operations
     */
    public static void delete(String buildingCode, DBConnector.DBTask dbTask) {
        try {
            if (buildingExists(buildingCode)) {
                String sql = "DELETE FROM miniproj_buildings WHERE build_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, buildingCode);

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("Building " + buildingCode + " doesn't exist");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Used to check if a building exists
     * @param buildingCode The code for the building to be searched for
     * @return boolean of whether the building exists
     * @throws SQLException
     */
    private static boolean buildingExists(String buildingCode) throws SQLException {
        String sql = "SELECT * FROM miniproj_buildings WHERE build_code = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, buildingCode);

        ResultSet queryResults = preparedStatement.executeQuery();

        return queryResults.next();
    }
}
