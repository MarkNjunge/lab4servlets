package com.ammv.servlets.room;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.database.RoomHelper;
import com.ammv.models.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CreateRoom")
public class CreateRoom extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        String roomCode = request.getParameter("roomCode");
        if (roomCode == null){
            finish();
        }else {
            createRoom(roomCode);
        }
    }

    private void createRoom(String roomCode) {
        String roomCapacity = currentRequest.getParameter("roomCapacity");
        String roomType = currentRequest.getParameter("roomType");
        String buildingCode = currentRequest.getParameter("buildingCode");

        int roomTypeCode;
        if (roomType.equals("class")){
            roomTypeCode = 0;
        }else {
            roomTypeCode = 1;
        }

        RoomHelper.create(new Room(roomCode, buildingCode, Integer.parseInt(roomCapacity), roomTypeCode), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                currentRequest.setAttribute("actionCompleted", true);
                finish();
            }
        });
    }

    private void finish(){
        try {
            currentRequest.setAttribute("buildings", BuildingHelper.readAll(reason -> displayError(reason)));
            currentRequest.getRequestDispatcher("CreateRoom.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            displayError(e.getMessage());
        }
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("CreateRoom.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
