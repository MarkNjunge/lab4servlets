package com.ammv.servlets.room;

import com.ammv.database.BuildingHelper;
import com.ammv.database.RoomHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditRoom")
public class EditRoom extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setRoomsList();

        String roomCode = request.getParameter("roomCode");
        String buildingCode = request.getParameter("buildingCode");
        String roomCapacity = request.getParameter("roomCapacity");
        String roomType = request.getParameter("roomType");

        String button = request.getParameter("button");

        switch (button) {
            case "save":
                saveRoom(roomCode, buildingCode, roomCapacity, roomType);
                break;
            case "delete":
                deleteRoom(roomCode);
                break;
            default:
                request.setAttribute("actionType", "error");
                finish();
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setRoomsList();

        String roomCode = request.getParameter("roomCode");

        if (roomCode == null) {
            finish();
        } else {
            Room room = RoomHelper.readOne(roomCode, reason -> displayError(reason));
            request.setAttribute("selectedRoom", room);
            request.setAttribute("buildings", BuildingHelper.readAll(reason -> displayError(reason)));
            finish();
        }
    }

    private void saveRoom(String roomCode, String buildingCode, String roomCapacity, String roomType) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason + "(" + roomCode + ")");
            }

            @Override
            public void taskSuccessful() {
                setRoomsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "save");
                finish();
            }
        };

        int roomTypeCode;
        if (roomType.equals("class")){
            roomTypeCode = 0;
        }else {
            roomTypeCode = 1;
        }

        RoomHelper.update(new Room(roomCode, buildingCode, Integer.parseInt(roomCapacity), roomTypeCode), dbTask);
    }

    private void deleteRoom(String roomCode) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                setRoomsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "delete");
                finish();
            }
        };

        RoomHelper.delete(roomCode, dbTask);
    }

    private void setRoomsList() {
        currentRequest.setAttribute("rooms", RoomHelper.readAll(reason -> displayError(reason)));
    }

    private void finish(){
        try {
            setRoomsList();
            currentRequest.getRequestDispatcher("EditRoom.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            displayError(e.getMessage());
        }
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("EditRoom.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
