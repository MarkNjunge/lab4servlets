package com.ammv.servlets.building;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Building;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditBuilding")
public class EditBuilding extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;
        
        setBuildingsList();

        String newCode = request.getParameter("newCode");
        String newName = request.getParameter("newName");
        String button = request.getParameter("button");

        switch (button) {
            case "save":
                saveBuilding(newCode, newName);
                break;
            case "delete":
                deleteBuilding(newCode);
                break;
            default:
                request.setAttribute("actionType", "error");
                request.getRequestDispatcher("EditBuilding.jsp").forward(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setBuildingsList();

        String buildingCode = request.getParameter("buildingCode");

        if (buildingCode == null) {
            request.getRequestDispatcher("EditBuilding.jsp").forward(request, response);
        } else {
            Building building = BuildingHelper.readOne(buildingCode, reason -> displayError(reason));
            request.setAttribute("selectedBuilding", building);
            request.getRequestDispatcher("EditBuilding.jsp").forward(request, response);
        }
    }

    private void setBuildingsList() {
        currentRequest.setAttribute("buildings", BuildingHelper.readAll(reason -> displayError(reason)));
    }

    private void saveBuilding(String newCode, String newName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason + "(" + newCode + ")");
            }

            @Override
            public void taskSuccessful() {
                setBuildingsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "save");
                finish();
            }
        };

        BuildingHelper.update(new Building(newCode, newName), dbTask);
    }

    private void deleteBuilding(String newCode) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                setBuildingsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "delete");
                finish();
            }
        };
        
        BuildingHelper.delete(newCode, dbTask);
    }

    private void finish(){
        try {
            currentRequest.getRequestDispatcher("EditBuilding.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            displayError(e.getMessage());
        }
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("EditBuilding.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

}
