package com.ammv.servlets.building;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Building;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CreateBuilding")
public class CreateBuilding extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        String buildingCode = request.getParameter("buildingCode");
        if (buildingCode == null) {
            request.getRequestDispatcher("CreateBuilding.jsp").forward(request, response);
        } else {
            String buildingName = currentRequest.getParameter("buildingName");
            createBuilding(buildingCode, buildingName);
        }
    }

    private void createBuilding(String buildingCode, String buildingName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                try {
                    currentRequest.setAttribute("buildingAdded", true);
                    currentRequest.setAttribute("buildingCode", buildingCode);
                    currentRequest.setAttribute("buildingName", buildingName);
                    currentRequest.getRequestDispatcher("CreateBuilding.jsp").forward(currentRequest, currentResponse);
                } catch (ServletException | IOException e) {
                    displayError(e.getMessage());
                }
            }
        };

        BuildingHelper.createBuilding(new Building(buildingCode, buildingName), dbTask);
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("CreateBuilding.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
