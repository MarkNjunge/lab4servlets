package com.ammv.servlets;

import com.ammv.database.DBConnector;
import com.ammv.database.UsersHelper;
import com.ammv.utils.RandomUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "AuthenticationServlet")
public class AuthenticationServlet extends HttpServlet {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private String username;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.request = request;
        this.response = response;

        username = request.getParameter("username");
        String password = request.getParameter("password");

        String button = request.getParameter("button");

        switch (button) {
            case "login":
                String hash = RandomUtils.createHash(password);
                login(username, hash);
                break;
            case "register":
                hash = RandomUtils.createHash(password);
                register(username, hash);
                break;
            case "logout":
                logout();
        }

    }

    private DBConnector.DBTask dbTask = new DBConnector.DBTask() {
        @Override
        public void taskFailed(String reason) {
            System.out.println(reason);
            // TODO find a way of passing the error message
            try {
                response.sendRedirect(request.getContextPath() + "/login.jsp");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void taskSuccessful() {
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            try {
                response.sendRedirect(request.getContextPath() + "/landing.jsp");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    };

    private void register(String username, String hash) {
        UsersHelper.addUser(username, hash, dbTask);
    }

    private void login(String username, String hash) {
        UsersHelper.validateUser(username, hash, dbTask);
    }

    private void logout() throws IOException, ServletException {
        request.getSession().invalidate();
        response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
}
