package com.ammv.servlets.course;

import com.ammv.database.CourseHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Course;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditCourse")
public class EditCourse extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setCoursesList();

        String newCode = request.getParameter("newCode");
        String newName = request.getParameter("newName");
        String button = request.getParameter("button");

        switch (button) {
            case "save":
                saveCourse(newCode, newName);
                break;
            case "delete":
                deleteCourse(newCode);
                break;
            default:
                request.setAttribute("actionType", "error");
                request.getRequestDispatcher("EditCourse.jsp").forward(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setCoursesList();

        String courseCode = request.getParameter("courseCode");

        if (courseCode == null) {
            request.getRequestDispatcher("EditCourse.jsp").forward(request, response);
        } else {
            Course course = CourseHelper.readOne(courseCode, reason -> displayError(reason));
            request.setAttribute("selectedCourse", course);
            request.getRequestDispatcher("EditCourse.jsp").forward(request, response);
        }
    }

    private void setCoursesList() {
        currentRequest.setAttribute("courses", CourseHelper.readAll(reason -> displayError(reason)));
    }

    private void saveCourse(String newCode, String newName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason + "(" + newCode + ")");
            }

            @Override
            public void taskSuccessful() {
                setCoursesList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "save");
                finish();
            }
        };

        CourseHelper.update(new Course(newCode, newName), dbTask);
    }

    private void deleteCourse(String newCode) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                setCoursesList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "delete");
                finish();
            }
        };

        CourseHelper.delete(newCode, dbTask);
    }

    private void finish(){
        try {
            currentRequest.getRequestDispatcher("EditCourse.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            displayError(e.getMessage());
        }
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("EditCourse.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

}
