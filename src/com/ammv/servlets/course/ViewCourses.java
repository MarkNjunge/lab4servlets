package com.ammv.servlets.course;

import com.ammv.database.CourseHelper;
import com.ammv.database.DBConnector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ViewCourses")
public class ViewCourses extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        request.setAttribute("courses", CourseHelper.readAll(reason -> displayError(reason)));

        request.getRequestDispatcher("ViewCourses.jsp").forward(request, response);
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("ViewCourse.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
