package com.ammv.servlets.course;

import com.ammv.database.CourseHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Course;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "CreateCourse")
public class CreateCourse extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        String courseCode = request.getParameter("courseCode");
        if (courseCode == null) {
            request.getRequestDispatcher("CreateCourse.jsp").forward(request, response);
        } else {
            String courseName = currentRequest.getParameter("courseName");
            createCourse(courseCode, courseName);
        }
    }

    private void createCourse(String courseCode, String courseName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                try {
                    currentRequest.setAttribute("courseAdded", true);
                    currentRequest.setAttribute("courseCode", courseCode);
                    currentRequest.setAttribute("courseName", courseName);
                    currentRequest.getRequestDispatcher("CreateCourse.jsp").forward(currentRequest, currentResponse);
                } catch (ServletException | IOException e) {
                    displayError(e.getMessage());
                }
            }
        };

        CourseHelper.create(new Course(courseCode, courseName), dbTask);
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("CreateCourse.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
