package com.ammv.servlets.unit;

import com.ammv.database.UnitHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Unit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "CreateUnit")
public class CreateUnit extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        String unitCode = request.getParameter("unitCode");
        if (unitCode == null) {
            request.getRequestDispatcher("CreateUnit.jsp").forward(request, response);
        } else {
            String unitName = currentRequest.getParameter("unitName");
            createUnit(unitCode, unitName);
        }
    }

    private void createUnit(String unitCode, String unitName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                try {
                    currentRequest.setAttribute("unitAdded", true);
                    currentRequest.setAttribute("unitCode", unitCode);
                    currentRequest.setAttribute("unitName", unitName);
                    currentRequest.getRequestDispatcher("CreateUnit.jsp").forward(currentRequest, currentResponse);
                } catch (ServletException | IOException e) {
                    displayError(e.getMessage());
                }
            }
        };

        UnitHelper.create(new Unit(unitCode, unitName), dbTask);
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("CreateUnit.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
