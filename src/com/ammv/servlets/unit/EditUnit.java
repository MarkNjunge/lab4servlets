package com.ammv.servlets.unit;

import com.ammv.database.UnitHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Unit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditUnit")
public class EditUnit extends HttpServlet {
    private HttpServletRequest currentRequest;
    private HttpServletResponse currentResponse;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setUnitsList();

        String newCode = request.getParameter("newCode");
        String newName = request.getParameter("newName");
        String button = request.getParameter("button");

        switch (button) {
            case "save":
                saveUnit(newCode, newName);
                break;
            case "delete":
                deleteUnit(newCode);
                break;
            default:
                request.setAttribute("actionType", "error");
                request.getRequestDispatcher("EditUnit.jsp").forward(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentRequest = request;
        currentResponse = response;

        setUnitsList();

        String unitCode = request.getParameter("unitCode");

        if (unitCode == null) {
            request.getRequestDispatcher("EditUnit.jsp").forward(request, response);
        } else {
            Unit unit = UnitHelper.readOne(unitCode, reason -> displayError(reason));
            request.setAttribute("selectedUnit", unit);
            request.getRequestDispatcher("EditUnit.jsp").forward(request, response);
        }
    }

    private void setUnitsList() {
        currentRequest.setAttribute("units", UnitHelper.readAll(reason -> displayError(reason)));
    }

    private void saveUnit(String newCode, String newName) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason + "(" + newCode + ")");
            }

            @Override
            public void taskSuccessful() {
                setUnitsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "save");
                finish();
            }
        };

        UnitHelper.update(new Unit(newCode, newName), dbTask);
    }

    private void deleteUnit(String newCode) {
        DBConnector.DBTask dbTask = new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                displayError(reason);
            }

            @Override
            public void taskSuccessful() {
                setUnitsList();
                currentRequest.setAttribute("actionCompleted", true);
                currentRequest.setAttribute("actionType", "delete");
                finish();
            }
        };

        UnitHelper.delete(newCode, dbTask);
    }

    private void finish(){
        try {
            currentRequest.getRequestDispatcher("EditUnit.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            displayError(e.getMessage());
        }
    }

    private void displayError(String message) {
        try {
            currentRequest.setAttribute("showAlert", true);
            currentRequest.setAttribute("alertMessage", message);
            currentRequest.getRequestDispatcher("EditUnit.jsp").forward(currentRequest, currentResponse);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

}
