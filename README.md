# Lab 4 Servlets

## Setup guide

1. Download _mysql-connector-java-5.1.42-bin.jar_ and _jstl-1.2.jar_ from [here](https://bitbucket.org/MarkNjunge/lab4servlets/downloads/).

2. Clone or download the repository and open it in IntelliJ IDEA (Any IDE will work).

3. If a popup appears informing you that a web framework has been detected, follow it.

4. Mark the folder _src_ as _Sources Root_ (Right click -> Mark Directory as > Sources Root).

5. Go to _Project Settings > Libraries_ and add _mysql-connector-java-5.1.42-bin.jar_ and _jstl-1.2.jar_

6. Go to _Project Settings > Modules/ProjectName/Sources_ and add a new folder called _out_ which should be marked as _Excluded_.

7. Go to _Project Settings > Modules/ProjectName/Dependencies_ and add Tomcat as a Library.

8. Go to _Project Settings > Project_ and add the path to folder made in step 6 under _Project compiler output_.

9. Go to _Project Settings > Artifacts_ and add a new _Web Application: Exploded/From modules_

10. Add a new local Tomcat Server under run configurations. If an error _No artifacts marked for deployment_ appears, click fix.

11. Run the server and navigate to _http://localhost:8080/_