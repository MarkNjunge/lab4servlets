<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Course</title>
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styling.css">
    <script src="assets/js/modernizr.custom.js"></script>
</head>
<body>
<div class="contain">
    <div class="fs-form-wrap" id="fs-form-wrap">
        <div class="fs-title">
            <h1>Create course</h1>
            <a href="/">Home</a>
        </div>
        <form id="myForm" class="fs-form fs-form-full" autocomplete="off" action="CreateCourse">
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1">Course code</label>
                    <input class="fs-anim-lower" id="q1" name="courseCode" type="text" placeholder="MSB" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q2"
                           data-info="Preferably similar to the Building code">Course Name</label>
                    <input class="fs-anim-lower" id="q2" name="courseName" type="text" placeholder="MSB" required/>
                </li>
            </ol>
            <button class="fs-submit" type="submit">Submit</button>
        </form>
        <c:if test="${courseAdded}">
            <div id="successModal" class="modal">
                <div class="modal-content">
                    A new Course has need added: ${courseCode}, ${courseName}
                </div>
            </div>
        </c:if>
    </div>
</div>
<c:if test="${courseAdded}">
    A new Course has need added: ${courseCode}, ${courseName}
</c:if>

<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/buildingScripts.js"></script>
</body>
</html>
