<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
    </head>
    <body>
    <div class="row">
        <div class="col offset-s4 s4">
            <div class="card-panel">
                <form action="auth" method="post">
                    <div class="row">
                        <div class="input-field">
                            <label for="username">Username: </label>
                            <input type="text" name="username" id="username">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s6">
                            <input class="waves-effect waves-light btn submitter" name="button" value="login" type="submit" >
                        </div>
                        <div class="col s6">
                            <input class="waves-effect waves-light btn submitter" type="submit" name="button" value="register">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </body>
</html>
