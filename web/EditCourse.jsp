<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Edit Course</title>
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styling.css">
    <script src="assets/js/modernizr.custom.js"></script>
</head>
<body>
<form action="EditCourse" method="get">
    <label style="font-size: 35px">Course code</label>
    <select name="buildingCode" style="width: 200px; height: 30px; font-size: 20px;">
        <option value="">Code</option>
        <c:forEach var="current" items="${courses}">
            <option value="${current.getCode()}">${current.getCode()}</option>
        </c:forEach>
    </select>
    <button type="submit" class="custombtn">submit</button>
</form>

<c:if test="${selectedCourse != null}">
    <form class="fs-form fs-form-full" autocomplete="off" action="EditCourse" method="post">
        <ol style="text-decoration: none">
            <li>
                <label class="fs-field-label fs-anim-upper" for="q1"> Code:</label>
                <input class="fs-anim-lower" id="q1" type="text" value="${selectedCourse.getCode()}" name="newCode" readonly>
            </li>
            <li>
                <label class="fs-field-label fs-anim-upper" for="q2"> Name:</label>
                <input class="fs-anim-lower" id="q2"  type="text" value="${selectedCourse.getName()}" name="newName">
            </li>
        </ol>
        <button type="submit" name="button" value="save">Save</button>
        <button type="submit" name="button" value="delete">Delete</button>
    </form>
</c:if>

<c:if test="${actionCompleted}">
    <c:choose>
        <c:when test="${actionType == 'save'}">
            <script>alert("Your changes have been saved.");</script>
        </c:when>
        <c:when test="${actionType == 'delete'}">
            <script>alert("The course has been deleted.");</script>
        </c:when>
    </c:choose>
</c:if>

<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/js/buildingScripts.js"></script>
</body>
</html>
