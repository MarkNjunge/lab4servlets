<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>$Title$</title>
        <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col s6">
                <ul class="collection with-header">
                    <li class="collection-header">Buildings</li>
                    <li class="collection-item"><a href="CreateBuilding">Create building</a></li>
                    <li class="collection-item"><a href="ViewBuildings">View buildings</a></li>
                    <li class="collection-item"><a href="EditBuilding">Edit buildings</a></li>
                </ul>
                <ul class="collection with-header">
                    <li class="collection-header">Rooms</li>
                    <li class="collection-item"><a href="CreateRoom">Create Room</a></li>
                    <li class="collection-item"><a href="ViewRooms">View Rooms</a></li>
                    <li class="collection-item"><a href="EditRoom">Edit Rooms</a></li>
                </ul>
            </div>
            <div class="col s6">
                <ul class="collection with-header">
                    <li class="collection-header">Courses</li>
                    <li class="collection-item"><a href="CreateCourse">Create Course</a></li>
                    <li class="collection-item"><a href="ViewCourses">View Courses</a></li>
                    <li class="collection-item"><a href="EditCourse">Edit Courses</a></li>
                </ul>
                <ul class="collection with-header">
                    <li class="collection-header">Units</li>
                    <li class="collection-item"><a href="CreateUnit">Create Unit</a></li>
                    <li class="collection-item"><a href="ViewUnits">View Units</a></li>
                    <li class="collection-item"><a href="EditUnit">Edit Units</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 center-align">
            <form method="post" action="auth">
                <input class="btn" type="submit" name="button" value="logout">
            </form>
        </div>
    </div>
    </body>
</html>
