<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>View Rooms</title>
        <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
    </head>
    <body>
    <div class="container">
        <table class="striped">
            <tr>
                <th class="center-align">Building code</th>
                <th class="center-align">Room Code</th>
                <th class="center-align">Room Capacity</th>
                <th class="center-align">Room Type</th>
            </tr>
            <c:forEach var="current" items="${rooms}">
                <tr>
                    <td class="center-align">${current.getBuildingCode()}</td>
                    <td class="center-align">${current.getCode()}</td>
                    <td class="center-align">${current.getCapacity()}</td>
                    <c:choose>
                        <c:when test="${current.isLab() == 1}">
                            <td>Lab</td>
                        </c:when>
                        <c:otherwise>
                            <td>Classroom</td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
    </div>

        <c:if test="${showAlert}">
            <script>alert("${alertMessage}");</script>
        </c:if>
    </body>
</html>
