(function() {
    var formWrap = document.getElementById( 'fs-form-wrap' );
    new FForm( formWrap, {
        onReview : function() {
            $('body').addClass('overview');
        }
    });
})();