<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Edit Room</title>
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/css/cs-select.css">
    <link rel="stylesheet" type="text/css" href="assets/css/cs-skin-boxes.css">
    <link rel="stylesheet" href="assets/css/styling.css">
    <script src="assets/js/modernizr.custom.js"></script>
</head>
<body>
<form action="EditRoom" method="get">
    <label style="font-size: 35px">Room code</label>
        <select name="roomCode" style="width: 200px; height: 30px; font-size: 20px;">
            <c:forEach var="current" items="${rooms}">
                <option value="${current.getCode()}">${current.getCode()}</option>
                <!-- Note the quotes -->
            </c:forEach>
        </select>
    <button type="submit" class="custombtn">submit</button>
</form>

<c:if test="${selectedRoom != null}">
    <form class="fs-form fs-form-full" autocomplete="off" action="EditRoom" method="post">
        <ol>
            <li>
                <label class="fs-field-label fs-anim-upper" for="q1">Room code: </label>
                <input class="fs-anim-lower" id="q1" name="roomCode" type="number" readonly value="${selectedRoom.getCode()}"/>
            </li>
            <li>
                <label for="select1">Building code</label>
                    <select id="select1" name="buildingCode" class="cs-select cs-skin-boxes fs-anim-lower">
                        <c:forEach var="current" items="${buildings}">
                            <c:choose>
                                <c:when test="${selectedRoom.getBuildingCode() == current.getCode()}">
                                    <option value="${current.getCode()}" selected>${current.getCode()}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${current.getCode()}">${current.getCode()}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
            </li>
            <li>
                <label class="fs-field-label fs-anim-upper" for="q2">Room capacity</label>
                <input class="fs-anim-lower" id="q2" name="roomCapacity" type="number" value="${selectedRoom.getCapacity()}" required/>
            </li>
            <li>
                <label for="type">Room type:</label>
                    <select id="type" name="roomType" class="cs-select cs-skin-boxes fs-anim-lower">
                        <c:choose>
                            <c:when test="${selectedRoom.isLab() == 1}">
                                <option value="class">Classroom</option>
                                <option value="lab" selected>Lab</option>
                            </c:when>
                            <c:otherwise>
                                <option value="class" selected>Classroom</option>
                                <option value="lab">Lab</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
            </li>
        </ol>
        <button type="submit" name="button" value="save">Save</button>
        <button type="submit" name="button" value="delete">Delete</button>
    </form>
</c:if>

<c:if test="${actionCompleted}">
    <c:choose>
        <c:when test="${actionType == 'save'}">
            <script>alert("Your changes have been saved.");</script>
        </c:when>
        <c:when test="${actionType == 'delete'}">
            <script>alert("The room has been deleted.");</script>
        </c:when>
    </c:choose>
</c:if>

<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
<script src="assets/js/classie.js"></script>
<script src="assets/js/selectFx.js"></script>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/js/buildingScripts.js"></script>
</body>
</html>