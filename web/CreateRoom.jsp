<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Create Room</title>
        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="assets/css/cs-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/cs-skin-boxes.css">
        <link rel="stylesheet" type="text/css" href="assets/css/styling.css">
        <script src="assets/js/modernizr.custom.js"></script>
    </head>
    <body>
    <div class="contain">
        <div class="fs-form-wrap" id="fs-form-wrap">
            <div class="fs-title">
                <h1>Create Room</h1>
                <a href="/">Home</a>
            </div>
        <form action="CreateRoom" method="get" class="fs-form fs-form-full" >
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="slct1">Building code:</label>
                    <select id="slct1" name="buildingCode" class="cs-select cs-skin-boxes fs-anim-lower">
                        <c:forEach var="current" items="${buildings}">
                            <option value="${current.getCode()}">${current.getCode()}</option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1">Room code</label>
                    <input class="fs-anim-lower" id="q1" name="roomCode" type="text" placeholder="MSB" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q2">Room capacity</label>
                    <input class="fs-anim-lower" id="q2" name="roomCapacity" type="number" placeholder="MSB" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="slct">Room Type:</label>
                    <select id="slct" name="roomType" class="cs-select cs-skin-boxes fs-anim-lower">
                        <option value="class">Classroom</option>
                        <option value="lab">Lab</option>
                    </select>
                </li>
            </ol>
            <button class="fs-submit" type="submit">Submit</button>
        </form>
        <c:if test="${actionCompleted}">
            <script>alert("The room has been created.");</script>
        </c:if>
        </div>
    </div>

        <c:if test="${showAlert}">
            <script>alert("${alertMessage}");</script>
        </c:if>
<script src="assets/js/classie.js"></script>
<script src="assets/js/selectFx.js"></script>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/buildingScripts.js"></script>
    </body>
</html>
