<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Unit</title>
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styling.css">
    <script src="assets/js/modernizr.custom.js"></script>
</head>
<body>
<div class="contain">
    <div class="fs-form-wrap" id="fs-form-wrap">
        <div class="fs-title">
            <h1>Create Unit</h1>
            <a href="/">Home</a>
        </div>
        <form class="fs-form fs-form-full" autocomplete="off" action="CreateUnit">
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1">Unit code</label>
                    <input class="fs-anim-lower" id="q1" name="unitCode" type="text" placeholder="MSB" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q2"
                           data-info="Preferably similar to the Building code">Unit Name</label>
                    <input class="fs-anim-lower" id="q2" name="unitName" type="text" placeholder="MSB" required/>
                </li>
            </ol>
            <button class="fs-submit" type="submit">Submit</button>
        </form>
        <c:if test="${unitAdded}">
            <div id="successModal" class="modal">
                <div class="modal-content">
                    A new Unit has need added: ${unitCode}, ${unitName}
                </div>
            </div>
        </c:if>
    </div>
</div>
<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/buildingScripts.js"></script>
</body>
</html>
