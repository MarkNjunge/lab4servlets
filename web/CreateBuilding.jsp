<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create Building</title>
    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styling.css">
    <script src="assets/js/modernizr.custom.js"></script>
</head>
<body>
<div class="contain">
    <div class="fs-form-wrap" id="fs-form-wrap">
        <div class="fs-title">
            <h1>Create Building</h1>
            <a href="/">Home</a>
        </div>
        <form id="myForm" class="fs-form fs-form-full" autocomplete="off" action="CreateBuilding">
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1">Building code</label>
                    <input class="fs-anim-lower" id="q1" name="buildingCode" type="text" placeholder="MSB" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q2"
                           data-info="Preferably similar to the Building code">Building Name</label>
                    <input class="fs-anim-lower" id="q2" name="buildingName" type="text" placeholder="MSB" required/>
                </li>
            </ol>
            <button class="fs-submit" type="submit">Submit</button>
        </form>
        <c:if test="${buildingAdded}">
            <div id="successModal" class="modal">
                <div class="modal-content">
                    A new Building has need added: ${buildingCode}, ${buildingName}
                </div>
            </div>
        </c:if>
    </div>
</div>
<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
<script src="assets/js/jquery-3.2.0.js"></script>
<script src="assets/js/fullscreenForm.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/buildingScripts.js"></script>
</body>
</html>
