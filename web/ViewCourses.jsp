<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>View Courses</title>
    <link rel="stylesheet" href="assets/materialize/css/materialize.min.css">
</head>
<body>
<div class="container">
    <table class="striped">
        <tr>
            <th class="center-align">Course code</th>
            <th class="center-align">Course name</th>
        </tr>
        <c:forEach var="current" items="${courses}">
            <tr>
                <td class="center-align">${current.getCode()}</td>
                <td class="center-align">${current.getName()}</td>
            </tr>
        </c:forEach>
    </table>
</div>

<c:if test="${showAlert}">
    <script>alert("${alertMessage}");</script>
</c:if>
</body>
</html>
